import Vue from "vue"
import Vuex from "vuex"
import { FeathersVuex } from "../api"
import auth from "./store.auth"

Vue.use(Vuex)
Vue.use(FeathersVuex)

/*
 * If not building with SSR mode, you can
 * directly export the Store instantiation;
 *
 * The function below can be async too; either use
 * async/await or return a Promise which resolves
 * with the Store instance.
 */

const requireModule = require.context(
  // The path where the service modules live
  "./services",
  // Whether to look in subfolders
  false,
  // Only include .js files (prevents duplicate imports`)
  /.js$/
)

const servicePlugins = requireModule
  .keys()
  .map(modulePath => requireModule(modulePath).default)

import showcase from "./showcase"

export default function (/* { ssrContext } */) {
  const Store = new Vuex.Store({
    modules: {
      showcase
    },

    plugins: [...servicePlugins, auth],

    // enable strict mode (adds overhead!)
    // for dev mode only
    strict: process.env.DEV
  })

  return Store
}
