import { makeAuthPlugin } from "../api"

export default makeAuthPlugin({ userService: "users" })
